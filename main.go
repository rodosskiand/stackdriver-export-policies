package main

import (
    "fmt"
    "os"
    "os/exec"
    "gopkg.in/yaml.v2"
    "log"
    "strings"
    "io/ioutil"
)

type Police struct {
	Combiner   string `yaml:"combiner"`
	Conditions []struct {
		ConditionThreshold struct {
			Aggregations []struct {
				AlignmentPeriod  string `yaml:"alignmentPeriod"`
				PerSeriesAligner string `yaml:"perSeriesAligner"`
			} `yaml:"aggregations"`
			Comparison     string  `yaml:"comparison"`
			Duration       string  `yaml:"duration"`
			Filter         string  `yaml:"filter"`
			ThresholdValue float64 `yaml:"thresholdValue"`
			Trigger        struct {
				Count int `yaml:"count"`
			} `yaml:"trigger"`
		} `yaml:"conditionThreshold"`
		DisplayName string `yaml:"displayName"`
	} `yaml:"conditions"`
	DisplayName   string `yaml:"displayName"`
	Enabled        bool `yaml:"enabled"`
}


type Channel struct {
	DisplayName string `yaml:"displayName"`
	Enabled     bool   `yaml:"enabled"`
	Labels      struct {
		URL string `yaml:"url,omitempty"`
		EmailAddress string `yaml:"email_address,omitempty"`
		Number string `yaml:"number,omitempty"`
	} `yaml:"labels"`
	Type string `yaml:"type"`
}

func concat2builder(x, y string) string {
    var builder strings.Builder
    builder.Grow(len(x) + len(y)) // Только эта строка выделяет память
    builder.WriteString(x)
    builder.WriteString(y)
    return builder.String()
}

func backup(typeof string) []byte  {
	var result string
	var d, error []byte
	cmd := "gcloud alpha monitoring "+typeof+" list"
    out, err := exec.Command("bash","-c",cmd).Output()
    if err != nil {
    	log.Fatalf("error: %v", err)
  	}
  	policies := strings.Split(string(out), "---")
  	for index, police := range policies {
  		_ = index
  		if typeof == "policies"{
  			structPolice := Police {}	
  			error := yaml.Unmarshal([]byte(police), &structPolice)
  			if structPolice.DisplayName == ""{continue}
        	_ = error
        	d, error = yaml.Marshal(&structPolice)
  		}else{
  			structChannel := Channel {}	
  			error := yaml.Unmarshal([]byte(police), &structChannel)
  			if structChannel.DisplayName == ""{continue}
        	_ = error
        	d, error = yaml.Marshal(&structChannel)
  		}
        if error != nil {
                log.Fatalf("error: %v", error)
        }
        fmt.Printf("---\n%s\n\n", string(d))
        if len(result) > 0 {
        result = concat2builder(result, "\n---\n")}
        result = concat2builder(result, string(d))

	}
	return []byte(result)
    
}


func restore(file_opt string, typeof string)  {
	file := strings.Title(typeof)+".yaml"
	if len(file_opt) > 0 {
    	file = file_opt
  	}
  	data, err := ioutil.ReadFile(file)
	if err != nil {
		fmt.Println(err)
	}
	items := strings.Split(string(data), "---")
  	for index, item := range items {
  		_ = index
  		_ = ioutil.WriteFile("temp.yaml", []byte(item), 0644)
  		var cmd string
  		if typeof == "policies"{
			cmd = "gcloud alpha monitoring "+typeof+" create --policy-from-file=temp.yaml"}else
		{
			cmd = "gcloud alpha monitoring "+typeof+" create --channel-content-from-file=temp.yaml"}
	    out, err := exec.Command("bash","-c",cmd).Output()
	    if err != nil {
	    	log.Fatalf("error: %v", err)
	  	}
	  	_ = out
	    log.Println(typeof+ "has been successfully created")
	    _ = err
	}
	_, _ = exec.Command("bash","-c","rm temp.yaml").Output()
    
}

func backup_policies() {
	_ = ioutil.WriteFile("Policies.yaml", backup("policies"), 0644)    
}

func backup_channels()  {
	_ = ioutil.WriteFile("Channels.yaml", backup("channels"), 0644)    
}

func restore_policies(file_opt string)  {
	restore(file_opt, "policies")
    
}
func restore_channels(file_opt string)  {
    restore(file_opt ,"channels")
}


func main() {
	Args := os.Args
	if len(Args) < 2{
		log.Fatalln("Please specify directories as arguments")

	}
	if Args[1] == "backup_all"{
		backup_policies()
		backup_channels()
	}
	if Args[1] == "restore_all"{
		restore_policies("")
		restore_channels("")
	}
	if Args[1] == "backup_policies"{
		backup_policies()
	}
	if Args[1] == "backup_channels"{
		backup_channels()
	}

	if Args[1] == "restore_policies"{
		if len(Args) > 2{
			restore_policies(string(Args[2]))
		}else{
			restore_policies("")
		}
		
	}
	if Args[1] == "restore_channels"{
		if len(Args) > 2{
			restore_channels(string(Args[2]))
		}else{
			restore_channels("")
		}	
	}
	if Args[1] == "help"{
		fmt.Printf("backup_all\nrestore_all\nbackup_policies\nrestore_policies\nbackup_channels\nrestore_channels")
	}

}
